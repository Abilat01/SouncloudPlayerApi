//
//  UserManager.swift
//  SouncloudDemoTest
//
//  Created by Danya on 16.11.2021.
//

import Foundation

//userDefaults
class userData {
    static let shared = userData()
    
    let defaults = UserDefaults.standard
    let userKey = SettingsKey.users.rawValue
    enum SettingsKey: String {
        case users
    }
    
    var users: [User] {
        get {
            if let data = defaults.value(forKey: "Users") as? Data {
                return try! PropertyListDecoder().decode([User].self, from: data)
            } else {
                return [User]()
            }
        } set {
            if let data = try? PropertyListEncoder().encode(newValue) {
                defaults.set(data, forKey: "Users")
            }
        }
    }
    
    //save()
    func saveUser(login: String, password: String, email: String) {
        let user = User(login: login, password: password, email: email)
        users.insert(user, at: 0)
        
    }
    
}
