//
//  Alert.swift
//  SouncloudDemoTest
//
//  Created by Danya on 16.11.2021.
// если понадобиться еще где Alert, то воспользуюсь от сюда

import UIKit

extension UIViewController {
    
    func alertController(title: String, messege: String) {
        let alertController = UIAlertController(title: title, message: messege, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default)
        
        alertController.addAction(alertAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
}
