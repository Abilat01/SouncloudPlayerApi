//
//  ExtensionString.swift
//  SouncloudDemoTest
//
//  Created by Danya on 15.11.2021.
//

import Foundation

extension String {
    
    enum ValidTypes {
        case login
        case password
        case email
    }
    
    enum Regex: String {
        case login = "[a-zA-Z0-9]{3,35}" //без символов
        case password = "(?=.*[a-zA-Z])(?=.*[0-9]).{6,35}" //по классике наверное минимум 6 символов и одна буква
        case email = "[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\\.[a-zA-Z]{2,10}" //формат Danya@mail.ru
    }
    
    func isValid(validType: ValidTypes) -> Bool {
        let format = "SELF MATCHES %@"
        var regex = ""
        
        switch validType {
        case .login: regex = Regex.login.rawValue
        case .password: regex = Regex.password.rawValue
        case .email: regex = Regex.email.rawValue
        }
        
        return NSPredicate(format: format, regex).evaluate(with: self)
        
    }
}
