//
//  UserModel.swift
//  SouncloudDemoTest
//
//  Created by Danya on 16.11.2021.
//

import Foundation

struct User: Codable {
    let login: String
    let password: String
    let email: String
}
