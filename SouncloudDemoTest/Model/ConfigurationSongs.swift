//
//  ConfigurationSongs.swift
//  SouncloudDemoTest
//
//  Created by Danya on 17.11.2021.
//

import Foundation

class ConfigurationSongs {
    
    var songs = [Song]()
    
    func configureSong() {

        songs.append(Song(name: "Мы", trackName: "We", artistName: "Дайте Танк(!)", imageAlbum: "Cover1"))
        songs.append(Song(name: "Я", trackName: "Ya", artistName: "Дайте Танк(!)", imageAlbum: "Cover2"))
        songs.append(Song(name: "Альтернатива", trackName: "Alternativa", artistName: "Дайте Танк(!)", imageAlbum: "Cover3"))
        songs.append(Song(name: "Маленький", trackName: "Malenkiy", artistName: "Дайте Танк(!)", imageAlbum: "Cover4"))
        songs.append(Song(name: "Монополия", trackName: "Monopolia", artistName: "Дайте Танк(!)", imageAlbum: "Cover5"))
        songs.append(Song(name: "Оплачено", trackName: "Oplacheno", artistName: "Дайте Танк(!)", imageAlbum: "Cover6"))
        songs.append(Song(name: "Слова паразиты", trackName: "SlovaParazitty", artistName: "Дайте Танк(!)", imageAlbum: "Cover7"))
        songs.append(Song(name: "Крепость", trackName: "Crepost", artistName: "Дайте Танк(!)", imageAlbum: "Cover8"))
        songs.append(Song(name: "Утро", trackName: "Utro", artistName: "Дайте Танк(!)", imageAlbum: "Cover9"))
        songs.append(Song(name: "Вуаля", trackName: "Vualya", artistName: "Дайте Танк(!)", imageAlbum: "Cover10"))
        
    }
}
