//
//  AlbomModel.swift
//  SouncloudDemoTest
//
//  Created by Danya on 16.11.2021.
//

import Foundation

struct AlbumModel {
    let result: [Album]
}

struct Album: Decodable {
    let artistName: String
    let collectionName: String
    let artworkUrl100: String
}
