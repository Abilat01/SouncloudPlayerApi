//
//  Song.swift
//  SouncloudDemoTest
//
//  Created by Danya on 17.11.2021.
//

import Foundation

struct Song {
    
    let name: String
    let trackName: String
    let artistName: String
    let imageAlbum: String
    
}
