//
//  AuthViewController.swift
//  SouncloudDemoTest
//
//  Created by Danya on 13.11.2021.
// чет беда с заливкой на гит лаб

import UIKit

class AuthViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func createAccount(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "Register") else { return }
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
    }
    
    @IBAction func singIn(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(identifier: "LogIn") else { return }
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
    }
}
