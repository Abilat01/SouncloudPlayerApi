//
//  SongsViewController.swift
//  SouncloudDemoTest
//
//  Created by Danya on 13.11.2021.
//

import UIKit
import AVFoundation

class SongsViewController: UIViewController {
    
    var configurate = ConfigurationSongs()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var resultTF: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurate.configureSong()
        
    }
    
    private func textFieldSettings() {
        searchTF.layer.borderWidth = 2
        searchTF.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        searchTF.textColor = .lightGray
        searchTF.backgroundColor = .black
        searchTF.attributedPlaceholder = NSAttributedString(string: "Search",
                                                            attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray])
    }
    
}

//MARK: - TableView

extension SongsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return configurate.songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SongsTableCell
        
        let song = configurate.songs[indexPath.row]
        
        cell.nameMusicLabel.text = song.name
        cell.groupNameLabel.text = song.artistName
        cell.accessoryType = .disclosureIndicator
        
        cell.albomImage.image = UIImage(named: song.imageAlbum)
        cell.albomImage.layer.cornerRadius = cell.albomImage.frame.size.height / 2
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //показ Player
        let position = indexPath.row
        
        guard let vc = storyboard?.instantiateViewController(identifier: "Detail") as? DetailPlayerViewController else {
            return
        }
        vc.songs = configurate.songs
        vc.position = position
        present(vc, animated: true)
        
    }
    
}
