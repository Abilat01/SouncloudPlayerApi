//
//  LogInViewController.swift
//  SouncloudDemoTest
//
//  Created by Danya on 13.11.2021.
//
//Обязательно засунуть все это дело в крол что бы клавиатура не загараживала TF
//Вход будет по почте @

import UIKit

class LogInViewController: UIViewController {
    
    @IBOutlet weak var logInLabel: UILabel!
    
    @IBOutlet weak var loginTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginTF.delegate = self
        passwordTF.delegate = self
        
        textFielsdsSettings()
        
        logInButton.isEnabled = false
        loginTF.addTarget(self, action:  #selector(textFieldDidChange(_:)),  for:.editingChanged)
        passwordTF.addTarget(self, action:  #selector(textFieldDidChange(_:)),  for:.editingChanged)
        
    }
    
    private func textFielsdsSettings() {
        loginTF.layer.borderWidth = 2
        loginTF.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        loginTF.backgroundColor = .black
        loginTF.textColor = .white
        loginTF.attributedPlaceholder = NSAttributedString(string: "Login",
                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        passwordTF.layer.borderWidth = 2
        passwordTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        passwordTF.backgroundColor = .black
        passwordTF.textColor = .white
        passwordTF.isSecureTextEntry = true
        passwordTF.attributedPlaceholder = NSAttributedString(string: "Password",
                                                              attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray])
    }
    
    //бдим за состоянием textField
    @objc func textFieldDidChange(_ sender: UITextField) {
        if loginTF.text == "" || passwordTF.text == "" {
            logInButton.isEnabled = false
        }else{
            logInButton.isEnabled = true
        }
    }
    
    @IBAction func logInButton(_ sender: UIButton) {
        
        let email = loginTF.text ?? ""
        let password = passwordTF.text ?? ""
        let user = findUserData(email: email)
        
        if user == nil {
            logInLabel.text = "User is not found"
            logInLabel.textColor = .red
        } else if user?.password == password {
            guard let vc = storyboard?.instantiateViewController(identifier: "TabBar") else { return }
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true)
        } else {
            logInLabel.text = "The password is incorrect"
            logInLabel.textColor = .red
        }
    }
    
    private func findUserData(email: String) -> User? {
        let userData = userData.shared.users
        print("Смотрим на пользователей \(userData)")
        
        for user in userData {
            if user.email == email {
                return user
            }
        }
        return nil
    }
    
    @IBAction func tap(_ sender: UITapGestureRecognizer) {
        loginTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
    }
}

extension LogInViewController: UITextFieldDelegate {
    
}
