//
//  songsTableCell.swift
//  SouncloudDemoTest
//
//  Created by Danya on 13.11.2021.
//

import UIKit

class SongsTableCell: UITableViewCell {

    @IBOutlet weak var albomImage: UIImageView!
    @IBOutlet weak var nameMusicLabel: UILabel!
    @IBOutlet weak var groupNameLabel: UILabel!
    
}
