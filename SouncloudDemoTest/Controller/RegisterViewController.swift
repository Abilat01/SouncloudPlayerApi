//
//  RegisterViewController.swift
//  SouncloudDemoTest
//
//  Created by Danya on 14.11.2021.
//
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var createAccountLabel: UILabel!
    
    @IBOutlet weak var loginTF: UITextField!
    @IBOutlet weak var loginWarningLabel: UILabel!
    
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var passwordWarningLabel: UILabel!
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var emailWarningLabel: UILabel!
    
    @IBOutlet weak var registerButton: UIButton!
    
    let loginValidType: String.ValidTypes = .login
    let passwordValidType: String.ValidTypes = .password
    let emailValidType: String.ValidTypes = .email
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldsSettings()
        
        loginTF.delegate = self //рот его наоборот
        passwordTF.delegate = self
        emailTF.delegate = self
        
    }
    
    //настройка TF
    private func textFieldsSettings() {
        loginTF.layer.borderWidth = 2
        loginTF.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        loginTF.backgroundColor = .black
        loginTF.textColor = .white
        loginTF.attributedPlaceholder = NSAttributedString(string: "Login",
                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        passwordTF.layer.borderWidth = 2
        passwordTF.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        passwordTF.backgroundColor = .black
        passwordTF.textColor = .white
        passwordTF.isSecureTextEntry = true
        passwordTF.attributedPlaceholder = NSAttributedString(string: "Password",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        emailTF.layer.borderWidth = 2
        emailTF.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        emailTF.backgroundColor = .black
        emailTF.textColor = .white
        emailTF.attributedPlaceholder = NSAttributedString(string: "Email",
                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
    }
    
    //проверяем поля и сохраняем пользователя + Alert
    @IBAction func registerAction(_ sender: UIButton) {
        
        let loginText = loginTF.text ?? ""
        let passwordText = passwordTF.text ?? ""
        let emailText = emailTF.text ?? ""
        
        if loginText.isValid(validType: loginValidType)
            && passwordText.isValid(validType: passwordValidType)
            && emailText.isValid(validType: emailValidType) {
            
            userData.shared.saveUser(login: loginText, password: passwordText, email: emailText)
            
            createAccountLabel.text = "Registration complete"
            
            guard let vc = storyboard?.instantiateViewController(withIdentifier: "TabBar") else { return }
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true)
            
        } else {
            alertController(title: "AHTUNG!!!", messege: "Check all fields for relevance")
        }
    }
    
    //Закрытие клавиатуры по нажатию на экран
    @IBAction func tap(_ sender: UITapGestureRecognizer) {
        loginTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
        emailTF.resignFirstResponder()
    }
    
    //метод проверки валидности
    private func setTextFieldsSetting(textField: UITextField, label: UILabel, validType: String.ValidTypes, validMesseng: String,
                                      wrongMessang: String, string: String, range: NSRange) {
        let text = (textField.text ?? "") + string
        let result: String
        
        if range.length == 1 {
            let end = text.index(text.startIndex, offsetBy: text.count - 1)
            result = String(text[text.startIndex..<end])
        } else {
            result = text
        }
        textField.text = result
        
        if result.isValid(validType: validType) {
            label.text = validMesseng
            label.textColor = .green
        } else {
            label.text = wrongMessang
            label.textColor = .red
        }
    }
    
}

extension RegisterViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case loginTF: setTextFieldsSetting(textField: loginTF,
                                           label: loginWarningLabel,
                                           validType: loginValidType,
                                           validMesseng: "Login is valid",
                                           wrongMessang: "Only A-Z and 0-9 characters, min 3 characters",
                                           string: string,
                                           range: range)
            
        case passwordTF: setTextFieldsSetting(textField: passwordTF,
                                              label: passwordWarningLabel,
                                              validType: passwordValidType,
                                              validMesseng: "secure password",
                                              wrongMessang: "min 6 numbers and 1 characters",
                                              string: string,
                                              range: range)
            
        case emailTF: setTextFieldsSetting(textField: emailTF,
                                           label: emailWarningLabel,
                                           validType: emailValidType,
                                           validMesseng: "Email is valid",
                                           wrongMessang: "incorrect format email",
                                           string: string,
                                           range: range)
        default:
            break
        }
        
        return false
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        loginTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
        emailTF.resignFirstResponder()
        return true
    }
}
